import com.appsenseca.pageobjects.EmailHomePage;
import com.appsenseca.pageobjects.EmailViewPage;
import com.appsenseca.pageobjects.SignInPage;
import com.appsenseca.utils.WebUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

/**
 * Created by rajeshbhattacharjee on 2016-05-23.
 */
public class GmailTest {

    WebDriver driver = new FirefoxDriver();

    @Test
    public void GamilLoginPage() {

        // open gmail to browser
        SignInPage signInPage = WebUtils.gotosignInPage(driver);

        signInPage.fillInUsername(driver , "pallavi.bhattacharjee23@gmail.com");

        signInPage.clickNextButton(driver);

        //Syn Problem - wait

        signInPage.fillPassword(driver, "Littlerascal@2016");

        //Note: since after submitting the button it will open another page call email home page so we have to create an anothoer pageobject with signin page and pa
        EmailHomePage emailHomePage = signInPage.clickSignIn(driver);

        //Verify user did sign in
        Assert.assertTrue("Failed to Locate Inbox Tab in the Page", signInPage.isInboxExist(driver));

        //Click signout
        signInPage = emailHomePage.signout(driver);

        Assert.assertTrue("Failed to SignOut ", emailHomePage.isSignOutButtontExist(driver));

    }

    @Test

    public void gmailSendRecieveMail(){

        // open gmail to browser
        SignInPage signInPage = WebUtils.gotosignInPage(driver);

        // userName.clear();
        signInPage.fillInUsername(driver , "pallavi.bhattacharjee23@gmail.com");

        //click Next Button
        signInPage.clickNextButton(driver);

        //Syn Problem - wait
        signInPage.fillPassword(driver, "Littlerascal@2016");

        // submit
        EmailHomePage emailHomePage = signInPage.clickSignIn(driver);

        //Assert.assertTrue("Failed to Locate Inbox Tab in the Page", driver.findElements(By.partialLinkText("Inbox")).size() > 0);

        //Click Compose Button
        emailHomePage.clickCompose(driver);

        //Fill Recipent email address
        emailHomePage.fillInRecipent(driver,"pallavi.bhattacharjee23@gmail.com");

        //Enter Subject Line
        final String subject = "Sending First Automated Mail from Selenium";
        emailHomePage.fillInSubjectLine(driver,subject);

        //Enter Email body part
        final String body = "Sending First Automated Mail from Selenium";
        emailHomePage.fillEmailBody(driver, body);

        //Click the Send Button
        emailHomePage.clickSendEmail(driver);

        //Click Inbox Tab again
        emailHomePage.clickNewMail(driver,"Inbox (1,010)");

        //Click email
        EmailViewPage emailViewPage = emailHomePage.clickTheMail(driver);

        //verify the inbox subject and body
        String actualSubject = emailViewPage.getEmailSubject(driver);

        Assert.assertEquals("Could Not find Required Subject", subject ,actualSubject);

        String actualMailBody = emailViewPage.getEmailBody(driver);

        Assert.assertEquals("Could Not find Required email body", body , actualMailBody);

        //SignOut
        emailHomePage.signout(driver);
    }

        @After
        public void teardown(){

            driver.quit();

        }

    }
