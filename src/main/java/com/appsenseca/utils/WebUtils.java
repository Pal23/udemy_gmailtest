package com.appsenseca.utils;

import com.appsenseca.pageobjects.SignInPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by rajeshbhattacharjee on 2016-05-25.
 */
public class WebUtils {

    final static int WAIT_TIME_OUT=30;

    public static SignInPage gotosignInPage(WebDriver driver) {
        driver.get("https://gmail.com");
        return PageFactory.initElements(driver ,SignInPage.class);
    }

    public static void click(WebDriver driver, By by) {  // Method for Click the elements
        WebElement element = driver.findElement(by);   //profileButton
        element.click();
    }

    public static void waitFortheElementVissible(WebDriver driver, By by) {  //methods for if Element is visisble
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_OUT);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public static boolean isElementExist(WebDriver driver, By by) { //Methods for if element exist
        return  driver.findElements(by).size() >0;
    }

    public static void clearAndSend(WebDriver driver, By by, String s) {
        WebElement element = driver.findElement(by);
        element.clear();
        element.sendKeys(s);
    }

    public static String getElementText(WebDriver driver, By by) {
        WebElement element = driver.findElement(By.cssSelector("h2[class='hP']"));
        return element.getText();
    }
}
