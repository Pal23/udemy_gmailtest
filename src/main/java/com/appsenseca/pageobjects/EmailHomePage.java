package com.appsenseca.pageobjects;

import com.appsenseca.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by rajeshbhattacharjee on 2016-05-26.
 */
public class EmailHomePage {

    public SignInPage signout(WebDriver driver) {
        WebUtils.click(driver, By.cssSelector("span[class='gb_3a gbii']"));

        WebUtils.click(driver, By.id("gb_71"));

        WebUtils.waitFortheElementVissible(driver, By.id("Email"));

        return PageFactory.initElements(driver , SignInPage.class);
    }

    public boolean isSignOutButtontExist(WebDriver driver) {

        return WebUtils.isElementExist(driver, By.cssSelector("h1[class='pYERqe']"));
    }

    public void clickCompose(WebDriver driver) {
        WebUtils.click(driver, By.cssSelector("div[role='button'][gh='cm']"));
        //WebElement composeButton = driver.findElement(By.cssSelector("div[role='button'][gh='cm']"));
        //composeButton.click();
    }

    public void fillInRecipent(WebDriver driver, String s) {
        WebUtils.waitFortheElementVissible(driver, By.cssSelector("textarea[name='to']"));

        WebUtils.clearAndSend(driver, By.cssSelector("textarea[name='to']"),"pallavi.bhattacharjee23@gmail.com");
    }

    public void fillInSubjectLine(WebDriver driver, String subject) {
        WebUtils.clearAndSend(driver,By.cssSelector("[placeholder='Subject']"), subject);
        //WebElement subjectTextArea = driver.findElement(By.cssSelector("[placeholder='Subject']"));
        //subjectTextArea.clear();
        //subjectTextArea.sendKeys(subject);
    }

    public void fillEmailBody(WebDriver driver, String body) {
        WebUtils.clearAndSend(driver,By.cssSelector("div[role='textbox']") ,body);
        //WebElement bodyTextArea = driver.findElement(By.cssSelector("div[role='textbox']"));
        //bodyTextArea.clear();
        //bodyTextArea.sendKeys(body);
    }

    public void clickSendEmail(WebDriver driver) {
        WebUtils.click(driver, By.cssSelector("div[aria-label*='Send']"));
        //WebElement sendButton = driver.findElement(By.cssSelector("div[aria-label*='Send']"));
        //sendButton.click();
    }

    public void clickNewMail(WebDriver driver, String s) {
        WebUtils.waitFortheElementVissible(driver,By.linkText("Inbox (1,010)"));
        //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Inbox (1,010)")));
        WebUtils.click(driver,By.linkText("Inbox (1,010)"));
       //WebElement loginLinkage = driver.findElement(By.linkText("Inbox (1,010)"));
        //loginLinkage.click();

    }

    public EmailViewPage clickTheMail(WebDriver driver) {
        WebUtils.waitFortheElementVissible(driver, By.cssSelector("div[class='y6'] span[id] b"));
        //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='y6'] span[id] b")));
        WebUtils.click(driver, By.cssSelector("div[class='y6'] span[id] b"));
       //WebElement newEmail = driver.findElement(By.cssSelector("div[class='y6'] span[id] b"));
        //newEmail.click();

        return PageFactory.initElements(driver, EmailViewPage.class);
    }
}
