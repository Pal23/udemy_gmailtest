package com.appsenseca.pageobjects;

import com.appsenseca.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by rajeshbhattacharjee on 2016-05-25.
 */
public class SignInPage {



    public void fillInUsername(WebDriver driver, String s) {
        WebUtils.clearAndSend(driver,By.xpath("//*[@id='Email']"),"pallavi.bhattacharjee23@gmail.com");
        //WebElement userName = driver.findElement(By.xpath("//*[@id='Email']"));
        //userName.clear();
        //userName.sendKeys("pallavi.bhattacharjee23@gmail.com");
    }

    public void clickNextButton(WebDriver driver) {
        WebUtils.click(driver, By.xpath("//*[@id='next']"));
       // WebElement submit = driver.findElement(By.xpath("//*[@id='next']"));
        //submit.submit();
        WebUtils.waitFortheElementVissible(driver,By.id("Passwd"));
        //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Passwd")));
    }

    public void fillPassword(WebDriver driver, String s) {
        WebUtils.clearAndSend(driver,By.id("Passwd"),s);
        //WebElement pwd = driver.findElement(By.id("Passwd"));
        //pwd.clear();
        //pwd.sendKeys(s);
    }

    public EmailHomePage clickSignIn(WebDriver driver) {
        WebUtils.click(driver, By.id("signIn"));
        //WebElement signInButton = driver.findElement(By.id("signIn"));
        //signInButton.click();

        WebUtils.waitFortheElementVissible(driver,By.partialLinkText("Inbox"));
       //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.partialLinkText("Inbox")));

        return PageFactory.initElements(driver, EmailHomePage.class);

    }

    public boolean isInboxExist(WebDriver driver) {
        return WebUtils.isElementExist(driver,By.partialLinkText("Inbox"));
       // return driver.findElements(By.partialLinkText("Inbox")).size() > 0;

    }
}
