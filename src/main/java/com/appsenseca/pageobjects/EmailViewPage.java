package com.appsenseca.pageobjects;

import com.appsenseca.utils.WebUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by rajeshbhattacharjee on 2016-05-26.
 */
public class EmailViewPage {
    public String getEmailSubject(WebDriver driver) {
        return WebUtils.getElementText(driver, By.cssSelector("h2[class='hP']"));

    }

    public String getEmailBody(WebDriver driver) {
        return WebUtils.getElementText(driver, By.cssSelector("div[class='nH aHU'] div[dir='ltr']"));
        //WebElement emailBodyArea = driver.findElement(By.cssSelector("div[class='nH aHU'] div[dir='ltr']"));
        //return emailBodyArea.getText();
    }
}
